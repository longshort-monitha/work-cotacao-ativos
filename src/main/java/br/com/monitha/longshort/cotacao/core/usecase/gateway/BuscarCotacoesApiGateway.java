package br.com.monitha.longshort.cotacao.core.usecase.gateway;

import java.time.LocalDate;

import br.com.monitha.longshort.cotacao.core.usecase.dto.HistoricoCotacaoResponse;

/**
 * Buscar cotações 
 * @author monic
 */
public interface BuscarCotacoesApiGateway {
	
	HistoricoCotacaoResponse buscar(String codigo, LocalDate inicio, LocalDate fim, boolean intraday);

}
