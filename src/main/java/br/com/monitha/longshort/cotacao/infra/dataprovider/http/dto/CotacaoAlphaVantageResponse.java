package br.com.monitha.longshort.cotacao.infra.dataprovider.http.dto;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CotacaoAlphaVantageResponse {
	
	@JsonProperty(value = "Meta Data")
	private CotacaoAlphaVantageMetaData metadata;
	
	@JsonProperty(value = "Time Series (Daily)")
	private Map<String, CotacaoAlphaVantageData> timeseries;
	
	public CotacaoAlphaVantageMetaData getMetadata() {
		return metadata;
	}
	public void setMetadata(CotacaoAlphaVantageMetaData metadata) {
		this.metadata = metadata;
	}
	public Map<String, CotacaoAlphaVantageData> getTimeseries() {
		return timeseries;
	}
	public void setTimeseries(Map<String, CotacaoAlphaVantageData> timeseries) {
		this.timeseries = timeseries;
	}
	
	

}
