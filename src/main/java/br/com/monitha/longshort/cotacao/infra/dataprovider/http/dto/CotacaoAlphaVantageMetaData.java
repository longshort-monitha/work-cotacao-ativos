package br.com.monitha.longshort.cotacao.infra.dataprovider.http.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CotacaoAlphaVantageMetaData {
	
	@JsonProperty(value = "1. Information")
	private String informacao;
	
	@JsonProperty(value = "2. Symbol")
	private String simbolo;
	
	@JsonProperty(value = "3. Last Refreshed")
	private String ultimaAtualizacao;
	
	@JsonProperty(value = "4. Output Size")
	private String tamanhoRetorno;
	
	@JsonProperty(value = "5. Time Zone")
	private String timezone;
	
	public String getInformacao() {
		return informacao;
	}
	public void setInformacao(String informacao) {
		this.informacao = informacao;
	}
	public String getSimbolo() {
		return simbolo;
	}
	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}
	public String getUltimaAtualizacao() {
		return ultimaAtualizacao;
	}
	public void setUltimaAtualizacao(String ultimaAtualizacao) {
		this.ultimaAtualizacao = ultimaAtualizacao;
	}
	public String getTamanhoRetorno() {
		return tamanhoRetorno;
	}
	public void setTamanhoRetorno(String tamanhoRetorno) {
		this.tamanhoRetorno = tamanhoRetorno;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	
}
