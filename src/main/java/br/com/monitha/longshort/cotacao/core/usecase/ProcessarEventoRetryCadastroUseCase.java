package br.com.monitha.longshort.cotacao.core.usecase;

import br.com.monitha.longshort.base.usecase.BaseUseCase;
import br.com.monitha.longshort.cotacao.core.usecase.dto.AtivoRequest;

public interface ProcessarEventoRetryCadastroUseCase extends BaseUseCase<AtivoRequest, Void>{

}
