package br.com.monitha.longshort.cotacao.infra.dataprovider.http;

import java.time.LocalDate;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import br.com.monitha.longshort.base.dto.response.ListaErroEnum;
import br.com.monitha.longshort.base.dto.response.ResponseData;
import br.com.monitha.longshort.base.dto.response.ResponseDataErro;
import br.com.monitha.longshort.cotacao.core.entity.HistoricoCotacao;
import br.com.monitha.longshort.cotacao.core.usecase.dto.HistoricoCotacaoResponse;
import br.com.monitha.longshort.cotacao.core.usecase.gateway.BuscarCotacoesApiGateway;
import br.com.monitha.longshort.cotacao.infra.dataprovider.http.dto.CotacaoAlphaVantageResponse;
import br.com.monitha.longshort.cotacao.infra.dataprovider.http.enums.FuncaoAlphaVantageEnum;

public class CotacoesHttpDataProvider implements BuscarCotacoesApiGateway {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final AlphaVantageApi alphaVantageApi;
	
	@Value("#{app.alphavantage.apikey}")
	private String apikey;
	
	public CotacoesHttpDataProvider(AlphaVantageApi alphaVantageApi) {
		this.alphaVantageApi = alphaVantageApi;
	}

	@Override
	public HistoricoCotacaoResponse buscar(String codigo, LocalDate inicio, LocalDate fim, boolean intraday) {

		HistoricoCotacaoResponse historicoResponse = new HistoricoCotacaoResponse();
		
		try {
		
		CotacaoAlphaVantageResponse response = alphaVantageApi.buscarCotacoes(
					intraday ? FuncaoAlphaVantageEnum.TIME_SERIES_INTRADAY.toString() :
					FuncaoAlphaVantageEnum.TIME_SERIES_DAILY.toString(), codigo.concat(".SA"), apikey);
		
		response.getTimeseries().forEach((k,v) -> 
			historicoResponse.getHistoricos().add(new HistoricoCotacao(codigo,
					Double.parseDouble(v.getFechamento()),
					LocalDate.parse(k)))
		);
		
		}catch (Exception e) {
			logger.error("Ocorreu um erro ao chamar a AlphaVantageApi", e);
			ResponseData responseData = new ResponseData();
			responseData.adicionarErro(new ResponseDataErro(Objects.nonNull(e.getCause()) ? 
					e.getCause().getMessage() : e.getMessage(), ListaErroEnum.TIMEOUT));
			historicoResponse.setResponse(responseData);
		}
		
		return historicoResponse;
		
	}

}
