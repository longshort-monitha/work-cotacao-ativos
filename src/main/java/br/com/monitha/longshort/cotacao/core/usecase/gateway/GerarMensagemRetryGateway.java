package br.com.monitha.longshort.cotacao.core.usecase.gateway;

import br.com.monitha.longshort.cotacao.core.usecase.dto.AtivoRequest;

public interface GerarMensagemRetryGateway {
	
	void gerar(AtivoRequest request);

}
