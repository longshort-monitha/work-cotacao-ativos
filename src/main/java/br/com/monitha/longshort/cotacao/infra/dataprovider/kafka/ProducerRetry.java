package br.com.monitha.longshort.cotacao.infra.dataprovider.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import br.com.monitha.longshort.cotacao.core.usecase.dto.AtivoRequest;
import br.com.monitha.longshort.cotacao.core.usecase.gateway.GerarMensagemRetryGateway;

@Component
@Profile("prod")
public class ProducerRetry implements GerarMensagemRetryGateway {
	
	@Value(value="#{app.kafta.retry.cadastro.ativos.topic-name}")
	private String kafkaTopicName;
	
	private final KafkaTemplate<String, AtivoRequest> kafkaTemplate;
	
	public ProducerRetry(String kafkaTopicName, KafkaTemplate<String, AtivoRequest> kafkaTemplate) {
		super();
		this.kafkaTopicName = kafkaTopicName;
		this.kafkaTemplate = kafkaTemplate;
	}
	
	@Override
	public void gerar(AtivoRequest request) {
		kafkaTemplate.send(kafkaTopicName, request);
	}



}
