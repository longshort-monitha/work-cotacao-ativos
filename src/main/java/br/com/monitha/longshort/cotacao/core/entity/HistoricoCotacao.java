package br.com.monitha.longshort.cotacao.core.entity;

import java.time.LocalDate;
import java.time.LocalTime;

import br.com.monitha.longshort.base.entity.BaseEntity;

public class HistoricoCotacao extends BaseEntity{
	
	private String codigoAtivo;
	private Double valor;
	private LocalDate data;
	private LocalTime hora;
	
	public HistoricoCotacao() {
	}

	public HistoricoCotacao(String codigoAtivo, Double valor, LocalDate data) {
		super();
		this.codigoAtivo = codigoAtivo;
		this.valor = valor;
		this.data = data;
	}
	
	public String getCodigoAtivo() {
		return codigoAtivo;
	}
	public void setCodigoAtivo(String codigoAtivo) {
		this.codigoAtivo = codigoAtivo;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public LocalDate getData() {
		return data;
	}
	public void setData(LocalDate data) {
		this.data = data;
	}
	public LocalTime getHora() {
		return hora;
	}
	public void setHora(LocalTime hora) {
		this.hora = hora;
	}

}
