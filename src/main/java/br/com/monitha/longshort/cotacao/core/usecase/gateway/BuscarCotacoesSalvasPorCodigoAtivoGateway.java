package br.com.monitha.longshort.cotacao.core.usecase.gateway;

import br.com.monitha.longshort.cotacao.core.usecase.dto.HistoricoCotacaoResponse;

/**
 * Busca cotações de determinado ativo ja saldo
 * @author monic
 */
public interface BuscarCotacoesSalvasPorCodigoAtivoGateway {
	
	/**
	 * Busca a primeira cotação que foi encontrada para determinado ativo
	 * @param codigoAtivo
	 * @return
	 */
	HistoricoCotacaoResponse buscarPrimeiraCotacao(String codigoAtivo);

}
