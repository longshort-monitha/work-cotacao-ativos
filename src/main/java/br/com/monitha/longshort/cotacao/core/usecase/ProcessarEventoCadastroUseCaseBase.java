package br.com.monitha.longshort.cotacao.core.usecase;

import java.time.LocalDate;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.monitha.longshort.base.dto.response.ListaErroEnum;
import br.com.monitha.longshort.base.dto.response.ResponseDataErro;
import br.com.monitha.longshort.base.gateway.SalvarGateway;
import br.com.monitha.longshort.cotacao.core.entity.HistoricoCotacao;
import br.com.monitha.longshort.cotacao.core.usecase.dto.AtivoDeadLetterResponse;
import br.com.monitha.longshort.cotacao.core.usecase.dto.AtivoRequest;
import br.com.monitha.longshort.cotacao.core.usecase.dto.HistoricoCotacaoResponse;
import br.com.monitha.longshort.cotacao.core.usecase.gateway.BuscarCotacoesApiGateway;
import br.com.monitha.longshort.cotacao.core.usecase.gateway.BuscarCotacoesSalvasPorCodigoAtivoGateway;
import br.com.monitha.longshort.cotacao.core.usecase.gateway.ConfiguracaoGateway;
import br.com.monitha.longshort.cotacao.core.usecase.gateway.GerarMensagemDeadLetterGateway;

public abstract class ProcessarEventoCadastroUseCaseBase {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	protected final GerarMensagemDeadLetterGateway gerarMensagemDeadLetterGateway;
	protected final BuscarCotacoesSalvasPorCodigoAtivoGateway buscarCotacoesSalvasPorCodigoAtivoGateway;
	protected final SalvarGateway<HistoricoCotacao> salvarGateway;
	protected final BuscarCotacoesApiGateway buscarCotacoesGateway;
	protected final ConfiguracaoGateway configuracaoGateway;
	
	public ProcessarEventoCadastroUseCaseBase(GerarMensagemDeadLetterGateway gerarMensagemDeadLetterGateway,
			BuscarCotacoesSalvasPorCodigoAtivoGateway buscarCotacoesSalvasPorCodigoAtivoGateway,
			SalvarGateway<HistoricoCotacao> salvarGateway, BuscarCotacoesApiGateway buscarCotacoesGateway,
			ConfiguracaoGateway configuracaoGateway) {
		this.gerarMensagemDeadLetterGateway = gerarMensagemDeadLetterGateway;
		this.buscarCotacoesSalvasPorCodigoAtivoGateway = buscarCotacoesSalvasPorCodigoAtivoGateway;
		this.salvarGateway = salvarGateway;
		this.buscarCotacoesGateway = buscarCotacoesGateway;
		this.configuracaoGateway = configuracaoGateway;
	}

	public Void executar(AtivoRequest request) {
		logger.info(
				"Processando evento para carregar histórico de cotações após cadastro de ativo. Código do Ativo = {}",
				request.getCodigo());

		AtivoDeadLetterResponse response = new AtivoDeadLetterResponse(request.getCodigo(), request.getDescricao());
		validarCamposObrigatorios(request, response);

		if (!response.getResponse().getErros().isEmpty()) {
			logger.warn("Gerando mensagem de deadletter");
			gerarMensagemDeadLetterGateway.gerar(response);
			return null;
		}

		if (Objects.nonNull(buscarCotacoesSalvasPorCodigoAtivoGateway.buscarPrimeiraCotacao(request.getCodigo()))) {
			logger.warn("Ignorando mensagem duplicada para ativo {}", request.getCodigo());
			return null;
		}

		LocalDate dataFinal = LocalDate.now();
		LocalDate dataInicial = dataFinal.minusDays(configuracaoGateway.getQuantidadeDiasParaCotacaoNoCadastroAtivo());
		logger.info("Iniciando busca das cotações no período de {} até {}", dataInicial, dataFinal);
		HistoricoCotacaoResponse responseCotacao = buscarCotacoes(request, dataInicial, dataFinal, 0);

		if (responseCotacao.getResponse().getErros().isEmpty()) {
			responseCotacao.getHistoricos().forEach(salvarGateway::salvar);
		}

		return null;
	}
	
	protected abstract HistoricoCotacaoResponse buscarCotacoes(AtivoRequest request, LocalDate dataInicial, 
			LocalDate dataFinal, int tentativas); 

	private void validarCamposObrigatorios(AtivoRequest request, AtivoDeadLetterResponse response) {
		if (StringUtils.isBlank(request.getCodigo())) {
			String msg = "O campo código é obrigatório";
			logger.error(msg);
			response.getResponse().adicionarErro(new ResponseDataErro(msg, ListaErroEnum.CAMPOS_OBRIGATORIOS));
		}
	}

}

