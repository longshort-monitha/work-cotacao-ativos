package br.com.monitha.longshort.cotacao.infra.dataprovider.http.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CotacaoAlphaVantageData {
	
	@JsonProperty(value = "1. open")
	private String abertura;
	
	@JsonProperty(value = "2. high")
	private String maximo;
	
	@JsonProperty(value = "3. low")
	private String minimo;
	
	@JsonProperty(value = "4. close")
	private String fechamento;
	
	@JsonProperty(value = "5. volume")
	private String volume;

	public String getAbertura() {
		return abertura;
	}

	public void setAbertura(String abertura) {
		this.abertura = abertura;
	}

	public String getMaximo() {
		return maximo;
	}

	public void setMaximo(String maximo) {
		this.maximo = maximo;
	}

	public String getMinimo() {
		return minimo;
	}

	public void setMinimo(String minimo) {
		this.minimo = minimo;
	}

	public String getFechamento() {
		return fechamento;
	}

	public void setFechamento(String fechamento) {
		this.fechamento = fechamento;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}
	
	
}
