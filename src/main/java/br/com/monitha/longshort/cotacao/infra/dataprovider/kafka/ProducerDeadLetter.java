package br.com.monitha.longshort.cotacao.infra.dataprovider.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import br.com.monitha.longshort.cotacao.core.usecase.dto.AtivoDeadLetterResponse;
import br.com.monitha.longshort.cotacao.core.usecase.gateway.GerarMensagemDeadLetterGateway;

@Component
@Profile("prod")
public class ProducerDeadLetter implements GerarMensagemDeadLetterGateway {

	@Value(value="#{app.kafta.deadletter.cadastro.ativos.topic-name}")
	private String kafkaTopicName;
	
	private final KafkaTemplate<String, AtivoDeadLetterResponse> kafkaTemplate;
	
	public ProducerDeadLetter(String kafkaTopicName, KafkaTemplate<String,
			AtivoDeadLetterResponse> kafkaTemplate) {
		this.kafkaTopicName = kafkaTopicName;
		this.kafkaTemplate = kafkaTemplate;
	}

	@Override
	public void gerar(AtivoDeadLetterResponse response) {
		kafkaTemplate.send(kafkaTopicName, response);

	}

}
