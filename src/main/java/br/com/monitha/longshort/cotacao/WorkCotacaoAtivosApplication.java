package br.com.monitha.longshort.cotacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkCotacaoAtivosApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkCotacaoAtivosApplication.class, args);
	}

}
