package br.com.monitha.longshort.cotacao.infra.dataprovider.http;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.monitha.longshort.cotacao.infra.dataprovider.http.dto.CotacaoAlphaVantageResponse;

@FeignClient(url="https://www.alphavantage.co/" , name = "alphaVantageApi")
public interface AlphaVantageApi {
	
	@GetMapping(value ="query", produces = MediaType.APPLICATION_JSON_VALUE)
	CotacaoAlphaVantageResponse buscarCotacoes(@RequestParam("function") String funcao,
			@RequestParam("symbol") String simbolo, @RequestParam("apikey") String apikey);

}
