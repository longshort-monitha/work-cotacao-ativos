package br.com.monitha.longshort.cotacao.infra.configuracao;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import br.com.monitha.longshort.cotacao.core.usecase.gateway.ConfiguracaoGateway;

@Configuration
public class Configuracao implements ConfiguracaoGateway{
	
	@Value(value="#{app.qdt-dias-cotacao-cadastroativos}")
	private Integer qtdDiasCotacaoCadastroAtivo;

	@Override
	public Integer getQuantidadeDiasParaCotacaoNoCadastroAtivo() {
		return qtdDiasCotacaoCadastroAtivo;
	}

}
