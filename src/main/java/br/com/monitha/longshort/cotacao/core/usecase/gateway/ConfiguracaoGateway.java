package br.com.monitha.longshort.cotacao.core.usecase.gateway;

public interface ConfiguracaoGateway {

	/**
	 *
	 * @return {@link Integer}
	 * */
	Integer getQuantidadeDiasParaCotacaoNoCadastroAtivo();
}
