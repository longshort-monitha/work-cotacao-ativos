package br.com.monitha.longshort.cotacao.core.usecase.dto;

import br.com.monitha.longshort.base.dto.response.BaseResponse;

public class AtivoDeadLetterResponse extends BaseResponse{

	private String codigo;
	private String descricao;
	
	public AtivoDeadLetterResponse() {
		
	}
	
	public AtivoDeadLetterResponse(String codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}
