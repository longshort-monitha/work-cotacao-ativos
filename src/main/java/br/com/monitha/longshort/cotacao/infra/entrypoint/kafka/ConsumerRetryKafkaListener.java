package br.com.monitha.longshort.cotacao.infra.entrypoint.kafka;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import br.com.monitha.longshort.cotacao.core.usecase.ProcessarEventoCadastroUseCase;
import br.com.monitha.longshort.cotacao.core.usecase.dto.AtivoRequest;

@Component
@KafkaListener(topics = "${app.kafta.retry.cadastro.ativos.topic-name}")
public class ConsumerRetryKafkaListener {
	
	private final ProcessarEventoCadastroUseCase processarEventoCadastroUseCase;

	private static final long TEMPO_RETRY_MILISEGUNDOS = 5000;
	
	public ConsumerRetryKafkaListener(ProcessarEventoCadastroUseCase processarEventoCadastroUseCase) {
		this.processarEventoCadastroUseCase = processarEventoCadastroUseCase;
	}

	@KafkaHandler
	public void receberEventoCadastroAtivo(AtivoRequest request, Acknowledgment acknowledgment,
			@Header(KafkaHeaders.RECEIVED_TIMESTAMP) long timestamp) throws InterruptedException {
		
		long millisegundos = LocalDateTime.now().minus(timestamp, ChronoUnit.MILLIS).getSecond() * 1000l;
		
		if(millisegundos >= TEMPO_RETRY_MILISEGUNDOS) {
			processarEventoCadastroUseCase.executar(request);
			acknowledgment.acknowledge();	
		}else {
			Thread.sleep(TEMPO_RETRY_MILISEGUNDOS - millisegundos);
		} 
		
		
		
		
	}

}

