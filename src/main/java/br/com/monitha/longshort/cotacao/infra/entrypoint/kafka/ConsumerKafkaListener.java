package br.com.monitha.longshort.cotacao.infra.entrypoint.kafka;

import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import br.com.monitha.longshort.cotacao.core.usecase.ProcessarEventoCadastroUseCase;
import br.com.monitha.longshort.cotacao.core.usecase.dto.AtivoRequest;

@Component
@KafkaListener(topics = "${app.kafta.cadastro.ativos.topic-name}")
public class ConsumerKafkaListener {

	private final ProcessarEventoCadastroUseCase processarEventoCadastroUseCase;

	public ConsumerKafkaListener(ProcessarEventoCadastroUseCase processarEventoCadastroUseCase) {
		this.processarEventoCadastroUseCase = processarEventoCadastroUseCase;
	}

	@KafkaHandler
	public void receberEventoCadastroAtivo(AtivoRequest request, Acknowledgment acknowledgment) {
		processarEventoCadastroUseCase.executar(request);
		acknowledgment.acknowledge();
	}

}

