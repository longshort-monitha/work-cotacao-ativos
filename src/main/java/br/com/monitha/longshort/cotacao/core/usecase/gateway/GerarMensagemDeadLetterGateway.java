package br.com.monitha.longshort.cotacao.core.usecase.gateway;

import br.com.monitha.longshort.cotacao.core.usecase.dto.AtivoDeadLetterResponse;

public interface GerarMensagemDeadLetterGateway {
	
	void gerar(AtivoDeadLetterResponse response);

}
